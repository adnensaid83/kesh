import React from "react";
import { Maybe } from "graphql/jsutils/Maybe";
import { ButtonRecord, StatItemRecord } from "@/graphql/types/graphql";
import { gfsDidot } from "../Common/SectionTitle";
import { ReactMarkdown } from "react-markdown/lib/react-markdown";
import Link from "next/link";

type Props = {
  title: string;
  subtitle: Maybe<string>;
  button: Maybe<ButtonRecord>;
  statItem: Array<StatItemRecord>;
};
const StatSection = ({ title, subtitle, button, statItem }: Props) => {
  return (
    <div className="body-font mb-0 flex flex-col items-center justify-center bg-secondary text-primary">
      <div className="container mx-auto px-5 py-4 md:py-12">
        <div className="flex w-full flex-col pb-12 text-center">
          <h1
            className={`${gfsDidot.className} mb-4 text-center text-4xl font-bold text-primary md:text-6xl lg:mx-auto lg:max-w-2xl lg:uppercase `}
          >
            {title}
          </h1>
          <div className="mx-auto text-base leading-relaxed lg:w-2/3">
            <ReactMarkdown>{subtitle || ""}</ReactMarkdown>
          </div>
        </div>
        <div className="-m-4 flex flex-col flex-wrap items-center justify-center pb-12 text-center md:flex-row">
          {statItem.map((stat, index) => {
            return (
              <div key={index} className="w-4/5 p-4 text-primary md:w-1/4">
                <div className="flex flex-col items-center justify-center rounded-lg border-2 border-primary px-4 py-6 text-center text-primary">
                  <h2 className="title-font mt-8 text-6xl font-medium text-gray-800">
                    {stat.quantity}
                  </h2>
                  <p className="leading-relaxed text-primary">{stat.label}</p>
                </div>
              </div>
            );
          })}
        </div>
        <div className="flex justify-center">
          <Link
            href={button?.url!}
            className="group inline-block text-base font-semibold"
          >
            <div className="group relative flex items-center gap-1 overflow-hidden rounded-full border-2 border-primary bg-white px-16 py-3 text-white duration-300 after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:z-0 after:bg-primary group-hover:text-primary group-hover:after:top-full after:motion-safe:duration-300">
              <span className="relative z-[1] motion-safe:duration-100">
                {button?.label}
              </span>
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default StatSection;
