import Image from "next/image";
import Link from "next/link";
import SvgRenderer from "../Common/SvgRenderer";
import {
  ChangeLogRecord,
  FooterQuery,
  LegalPageRecord,
  SiteLocale,
} from "@/graphql/types/graphql";
import { notFound } from "next/navigation";
/* import { primaryColor } from "@/app/i18n/settings"; */
import ReactMarkdown from "react-markdown";
import { gfsDidot } from "../Common/SectionTitle";
const primaryColor = "#FFFFFF";
type Props = {
  data: FooterQuery;
  lng: SiteLocale;
};

const Footer = ({ data, lng }: Props) => {
  return (
    <footer className="relative z-10 mt-[6rem] bg-primary text-center lg:text-start">
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5571.907200533926!2d4.971953376227567!3d45.711967671079044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f4c423110b49f5%3A0x1d4bdddd87431bef!2s146%20Rte%20de%20Grenoble%2C%2069800%20Saint-Priest!5e0!3m2!1sfr!2sfr!4v1714673720493!5m2!1sfr!2sfr"
        width="100%"
        className="h-[920px] border-0 lg:h-[500px]"
        loading="lazy"
      ></iframe>
      <div className="absolute top-0 h-full w-full bg-[rgba(15,15,15,.8)]">
        <div className="bg-tertiary-light mx-6 max-w-5xl -translate-y-20 rounded-xl bg-primary px-4 py-4 text-white md:-translate-y-24 md:px-12 md:py-12 lg:-translate-y-20 xl:mx-auto">
          <h3
            className={`mb-4 text-2xl text-activeNav lg:uppercase ${gfsDidot.className}`}
          >
            À propos
          </h3>
          <p className="text-lg lg:text-xl">
            Nous nous engageons à créer une expérience unique et mémorable où
            chaque détail est soigneusement pensé pour répondre à vos attentes
            les plus exigeantes
          </p>
        </div>
        <div className="mx-auto w-full max-w-[1140px]">
          <div className="flex w-full flex-col justify-between px-12 lg:flex-row lg:gap-24">
            <div className="lg:w-1/3">
              <div className="mb-6 lg:mb-16">
                <Link
                  href={"/" + lng + "/accueil"}
                  className="inline-block lg:mb-8"
                >
                  {data.layout?.footerLogo && (
                    <Image
                      src={data.layout.footerLogo.url}
                      alt="logo"
                      className="w-full"
                      width={data.layout.footerLogo.width || 100}
                      height={data.layout.footerLogo.height || 100}
                    />
                  )}
                </Link>
                <div className="mb-9 text-base font-medium leading-relaxed text-white">
                  <ReactMarkdown>
                    {data.layout!.footerSubtitle || ""}
                  </ReactMarkdown>
                </div>
                <div className="flex items-center justify-center lg:justify-start">
                  {data.layout!.socialMediaLinks.map((socialMedia) => {
                    return (
                      <a
                        href={socialMedia.url}
                        target="_blank"
                        aria-label="social-link"
                        className="mr-6 text-white last:mr-0 hover:text-primary"
                        key={socialMedia.id}
                      >
                        <SvgRenderer url={socialMedia.icon.url} />
                      </a>
                    );
                  })}
                </div>
              </div>
            </div>
            <div className="flex flex-col items-center lg:w-1/3">
              <div>
                <h2
                  className={`${gfsDidot.className} mb-6 text-xl font-bold uppercase text-white dark:text-primary lg:mb-10`}
                >
                  {data.layout?.informations?.adresseLabel}
                </h2>
                <ul className="flex flex-col">
                  <li className="mb-4 inline-block text-base font-medium text-white">
                    {data.layout?.informations?.adresseValue}
                  </li>
                  <li className="mb-4 inline-block text-base font-medium text-white">
                    {data.layout?.informations?.phoneValue}
                  </li>
                  <li className="mb-8 inline-block text-base font-medium text-white lg:mb-4">
                    {data.layout?.informations?.emailValue}
                  </li>
                </ul>
              </div>
            </div>
            <div className="flex flex-col lg:w-1/3 lg:items-end">
              <div>
                <div className="mb-6 lg:mb-16">
                  <h2
                    className={`${gfsDidot.className} mb-6 text-xl font-bold uppercase text-white dark:text-primary lg:mb-10`}
                  >
                    Mentions Legales
                  </h2>
                  <ul>
                    {data.layout!.footerLinks.map((link) => {
                      const pageLink = link as LegalPageRecord; // The field has a "at least one" validation
                      return (
                        <li key={pageLink.id}>
                          <a
                            href={"/" + lng + "/legal/" + pageLink.slug}
                            className="mb-4 inline-block text-base font-medium text-white"
                          >
                            {pageLink.title}
                          </a>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <p className="text-center text-white">
            Made with DatoCMS - Conçu et réalisé par
            <Link href="https://adnensaid.fr" className="px-2 underline">
              Adnen
            </Link>
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
