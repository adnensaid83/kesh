import Blog from "../Blog";
import Brands from "../Home/Brands";
import Features from "../Home/Features";
import Hero from "../Home/Hero";
import Pricing from "../Home/Pricing";
import Testimonials from "../Home/Testimonials";
import Video from "../Home/Video";
import DetailSection from "../Home/Detail/DetailSection";
import CompactTeam from "../About/CompactTeam";
import ExpandedTeam from "../About/ExpandedTeam";
import FAQAccordion from "../About/FAQAccordion";
import FAQGrid from "../About/FAQGrid";
import StatsSection from "../About/StatsSection";
import AboutIntro from "../About/AboutIntro";
import { Image as DatoImage } from "react-datocms";
import {
  AboutIntroRecord,
  AllPostsSectionRecord,
  BrandSectionRecord,
  ChangelogSectionRecord,
  CollectionMetadata,
  ContactSectionRecord,
  DetailSectionRecord,
  FaqSectionRecord,
  FeatureListSectionRecord,
  FeaturedPostsSectionRecord,
  GaleriePhotoRecord,
  HeroSectionRecord,
  PageModelSectionsField,
  PostRecord,
  PrestationSectionRecord,
  PricingSectionRecord,
  RedirectSectionRecord,
  ReservationRecord,
  ReservationSectionRecord,
  ReviewSectionRecord,
  ServiceSectionRecord,
  SiteLocale,
  StatSectionRecord,
  StatsSectionRecord,
  TeamSectionRecord,
  VideoSectionHomeRecord,
  VideoSectionRecord,
} from "@/graphql/types/graphql";
import GradientHero from "../Home/Hero/GradientHero";
import FeatureCards from "../Home/Features/FeatureCards";
import PostGridRenderer from "../Blog/PostGridRenderer";
import { redirect } from "next/navigation";
import RightImageHero from "../Home/Hero/RightImageHero";
import BackgroundImageHero from "../Home/Hero/BackgroundImage";
import SplitImage from "../Home/Hero/SplitImage";
import GradientCards from "../Home/Pricing/GradientCards";
import Minimal from "../Home/Pricing/Minimal";
import FeatureListSelector from "../Home/Pricing/FeatureListSelector";
import SmallCards from "../Home/Pricing/SmallCards";
import Carrousel from "../Home/Testimonials/Carrousel";
import ModernCarrousel from "../Home/Testimonials/ModernCarrousel";
import MinimalCarrousel from "../Home/Testimonials/MinimalCarrousel";
import MinimalReviewCards from "../Home/Testimonials/MinimalReviewCards";
import BrandCards from "../Home/Brands/BrandCards";
import ModernPostCards from "../Home/Featured Posts/ModernPostCards";
import CarrouselFeaturedPosts from "../Home/Featured Posts/CarrouselFeaturedPosts";
import MinimalistFeaturedPostsGrid from "../Home/Featured Posts/MinimalistFeaturedPostsGrid";
import FullImageFeaturedPosts from "../Home/Featured Posts/FullImageFeaturedPosts";
import MinimalCardsFeature from "../Home/Features/MinimalCardsFeature";
import BigImageHorizontalFeatures from "../Home/Features/BigImageHorizontalFeatures";
import BigImageVerticalFeatures from "../Home/Features/BigImageVerticalFeatures";
import Changelog from "../Changelog";
import QuoteRequest from "../Reservation/ReservationRequest";
import { gfsDidot } from "../Common/SectionTitle";
import Link from "next/link";
import ServiceSection from "../Home/Services/ServiceSection";
import PrestationSection from "../Home/Prestation/PrestationSection";
import ContactSection from "../Home/Contact/ContactSection";
import Reservation from "../Reservation";
import ReactMarkdown from "react-markdown";
import StatSection from "../About/StatSection";
import GaleriePhoto from "../Home/GaleriePhoto/GaleriePhoto";
import VideoHome from "../Home/VideoHome";

type Props = {
  sections: Array<PageModelSectionsField>;
  locale: SiteLocale;
  posts: PostRecord[];
  postMeta: CollectionMetadata;
  tokenReservation: string;
  idReservation: string;
};

export default function Section({
  sections,
  locale,
  posts,
  postMeta,
  tokenReservation,
  idReservation,
}: Props) {
  return (
    <>
      {sections.map((section, index) => {
        switch (section._modelApiKey) {
          case "video_section_home":
            const videoSectionHomeRecord = section as VideoSectionHomeRecord;
            return (
              <VideoHome
                key={videoSectionHomeRecord.id}
                videoHeader={videoSectionHomeRecord.videoHeader}
                videoSubheader={videoSectionHomeRecord.videoSubheader}
                videoUid={videoSectionHomeRecord.video?.providerUid}
                videoThumbnail={videoSectionHomeRecord.videoThumbnail}
                videoProvider={videoSectionHomeRecord.video?.provider}
              />
            );
          case "video_section":
            const videoSectionRecord = section as VideoSectionRecord;
            return (
              <Video
                key={videoSectionRecord.id}
                videoHeader={videoSectionRecord.videoHeader}
                videoSubheader={videoSectionRecord.videoSubheader}
                videoUid={videoSectionRecord.video?.providerUid}
                videoThumbnail={videoSectionRecord.videoThumbnail}
                videoProvider={videoSectionRecord.video?.provider}
              />
            );
          case "galerie_photo":
            const galeriePhotoRecord = section as GaleriePhotoRecord;
            return (
              <GaleriePhoto
                key={galeriePhotoRecord.id}
                title={galeriePhotoRecord.title}
                images={galeriePhotoRecord.galerieImages}
                button={galeriePhotoRecord.buttonGalerie}
              />
            );
          case "hero_section":
            const heroSectionRecord = section as HeroSectionRecord;
            switch (heroSectionRecord.displayOptions) {
              case "gradient":
                return (
                  <GradientHero
                    key={heroSectionRecord.id}
                    heroTitle={heroSectionRecord.heroTitle}
                    heroSubtitle={heroSectionRecord.heroSubtitle}
                    buttons={heroSectionRecord.buttons}
                  />
                );
              case "right_image":
                return (
                  <RightImageHero
                    key={heroSectionRecord.id}
                    heroTitle={heroSectionRecord.heroTitle}
                    heroSubtitle={heroSectionRecord.heroSubtitle}
                    buttons={heroSectionRecord.buttons}
                    image={heroSectionRecord.heroImage}
                  />
                );
              case "background_image":
                return (
                  <BackgroundImageHero
                    key={heroSectionRecord.id}
                    heroTitle={heroSectionRecord.heroTitle}
                    heroSubtitle={heroSectionRecord.heroSubtitle}
                    buttons={heroSectionRecord.buttons}
                    image={heroSectionRecord.heroImage}
                  />
                );
              case "split_image":
                return (
                  <SplitImage
                    key={heroSectionRecord.id}
                    heroTitle={heroSectionRecord.heroTitle}
                    heroSubtitle={heroSectionRecord.heroSubtitle}
                    buttons={heroSectionRecord.buttons}
                    image={heroSectionRecord.heroImage}
                  />
                );
              default:
                return (
                  <Hero
                    key={heroSectionRecord.id}
                    heroTitle={heroSectionRecord.heroTitle}
                    heroSubtitle={heroSectionRecord.heroSubtitle}
                    buttons={heroSectionRecord.buttons}
                  />
                );
            }
          case "service_section":
            const serviceSectionRecord = section as ServiceSectionRecord;
            return (
              <ServiceSection
                key={serviceSectionRecord.id}
                title={serviceSectionRecord.title}
                service={serviceSectionRecord?.service}
              />
            );
          case "prestation_section":
            const prestationSection = section as PrestationSectionRecord;
            return (
              <PrestationSection
                key={prestationSection.id}
                title={prestationSection.title}
                content={prestationSection.content}
                smallTitle={prestationSection.smallTitle}
                prestationImage={prestationSection.prestationImage}
              />
            );
          case "contact_section":
            const contactSectionRecord = section as ContactSectionRecord;
            return (
              <ContactSection
                key={contactSectionRecord.id}
                adresseLabel={contactSectionRecord.adresseLabel}
                adresseValue={contactSectionRecord.adresseValue}
                contactLabel={contactSectionRecord.contactLabel}
                phoneValue={contactSectionRecord.phoneValue}
                emailValue={contactSectionRecord.emailValue}
                horaireLabel={contactSectionRecord.horaireLabel}
                rdvLabel={contactSectionRecord.rdvLabel}
                horaireValue={contactSectionRecord.horaireValue}
              />
            );
          case "reservation":
            const reservationSectionRecord = section as ReservationRecord;
            return (
              <div key={reservationSectionRecord.id}>
                <h1 className="text-4xl text-black">
                  {reservationSectionRecord.title}
                </h1>
              </div>
            );
          case "reservation_section":
            const reservation = section as ReservationSectionRecord;
            return (
              <Reservation
                key={reservation.id}
                title={reservation.reservationTitle}
                reservationImage={reservation.reservationImage}
                questions={reservation.questions}
                token={tokenReservation}
                idReservation={idReservation}
              />
            );
          case "review_section":
            const reviewSectionRecord = section as ReviewSectionRecord;
            switch (reviewSectionRecord.displayOptions) {
              case "card_carrousel":
                return (
                  <Carrousel
                    key={reviewSectionRecord.id}
                    header={reviewSectionRecord.reviewSectionHeader}
                    subheader={reviewSectionRecord.reviewSectionSubheader}
                    reviews={reviewSectionRecord.reviews}
                  />
                );
              case "modern_carrousel":
                return (
                  <ModernCarrousel
                    key={reviewSectionRecord.id}
                    header={reviewSectionRecord.reviewSectionHeader}
                    subheader={reviewSectionRecord.reviewSectionSubheader}
                    reviews={reviewSectionRecord.reviews}
                  />
                );
              case "minimal_carrousel":
                return (
                  <MinimalCarrousel
                    key={reviewSectionRecord.id}
                    header={reviewSectionRecord.reviewSectionHeader}
                    subheader={reviewSectionRecord.reviewSectionSubheader}
                    reviews={reviewSectionRecord.reviews}
                  />
                );
              case "minimal_cards":
                return (
                  <MinimalReviewCards
                    key={reviewSectionRecord.id}
                    header={reviewSectionRecord.reviewSectionHeader}
                    subheader={reviewSectionRecord.reviewSectionSubheader}
                    reviews={reviewSectionRecord.reviews}
                  />
                );
              default:
                return (
                  <Testimonials
                    key={reviewSectionRecord.id}
                    header={reviewSectionRecord.reviewSectionHeader}
                    subheader={reviewSectionRecord.reviewSectionSubheader}
                    reviews={reviewSectionRecord.reviews}
                  />
                );
            }
          case "changelog_section":
            const changeLogSection = section as ChangelogSectionRecord;
            return (
              <Changelog
                key={changeLogSection.id}
                title={changeLogSection.title}
                subtitle={changeLogSection.subtitle}
                featuredChangeLogs={changeLogSection.featuredVersions}
                locale={locale}
              />
            );

          case "feature_list_section":
            const featureListSectionRecord =
              section as FeatureListSectionRecord;
            switch (featureListSectionRecord.displayOption) {
              case "card_minimal":
                return (
                  <MinimalCardsFeature
                    key={featureListSectionRecord.id}
                    features={featureListSectionRecord.feature}
                    featuresHeader={featureListSectionRecord.featuresHeader}
                    featuresSubheader={
                      featureListSectionRecord.featuresSubheader
                    }
                  />
                );
              case "grid":
                return (
                  <Features
                    key={featureListSectionRecord.id}
                    features={featureListSectionRecord.feature}
                    featuresHeader={featureListSectionRecord.featuresHeader}
                    featuresSubheader={
                      featureListSectionRecord.featuresSubheader
                    }
                  />
                );
              case "big_image_horizontal":
                return (
                  <BigImageHorizontalFeatures
                    key={featureListSectionRecord.id}
                    features={featureListSectionRecord.feature}
                    featuresHeader={featureListSectionRecord.featuresHeader}
                    featuresSubheader={
                      featureListSectionRecord.featuresSubheader
                    }
                  />
                );
              case "big_image_vertical":
                return (
                  <BigImageVerticalFeatures
                    key={featureListSectionRecord.id}
                    features={featureListSectionRecord.feature}
                    featuresHeader={featureListSectionRecord.featuresHeader}
                    featuresSubheader={
                      featureListSectionRecord.featuresSubheader
                    }
                  />
                );
              default:
                return (
                  <FeatureCards
                    key={featureListSectionRecord.id}
                    features={featureListSectionRecord.feature}
                    featuresHeader={featureListSectionRecord.featuresHeader}
                    featuresSubheader={
                      featureListSectionRecord.featuresSubheader
                    }
                  />
                );
            }
          case "brand_section":
            const brandSectionRecord = section as BrandSectionRecord;
            switch (brandSectionRecord.displayOptions) {
              case "brand_cards":
                return (
                  <BrandCards
                    key={brandSectionRecord.id}
                    brandShowcase={brandSectionRecord.brand}
                  />
                );
              default:
                return (
                  <Brands
                    key={brandSectionRecord.id}
                    brandShowcase={brandSectionRecord.brand}
                  />
                );
            }
          case "detail_section":
            const detailSectionRecord = section as DetailSectionRecord;
            return (
              <DetailSection
                key={detailSectionRecord.id}
                imagePosition={detailSectionRecord.imagePosition as boolean}
                image={detailSectionRecord.image}
                details={detailSectionRecord.details}
              />
            );
          case "pricing_section":
            const pricingSectionRecord = section as PricingSectionRecord;
            switch (pricingSectionRecord.displayOption) {
              case "cards_gradient":
                return (
                  <GradientCards
                    key={pricingSectionRecord.id}
                    header={pricingSectionRecord.pricingSectionHeader}
                    subheader={pricingSectionRecord.pricingSectionSubheader}
                    plans={pricingSectionRecord.plans}
                  />
                );
              case "minimal":
                return (
                  <Minimal
                    key={pricingSectionRecord.id}
                    header={pricingSectionRecord.pricingSectionHeader}
                    subheader={pricingSectionRecord.pricingSectionSubheader}
                    plans={pricingSectionRecord.plans}
                  />
                );
              case "feature_list":
                return (
                  <FeatureListSelector
                    key={pricingSectionRecord.id}
                    header={pricingSectionRecord.pricingSectionHeader}
                    subheader={pricingSectionRecord.pricingSectionSubheader}
                    plans={pricingSectionRecord.plans}
                  />
                );
              case "mini_cards":
                return (
                  <SmallCards
                    key={pricingSectionRecord.id}
                    header={pricingSectionRecord.pricingSectionHeader}
                    subheader={pricingSectionRecord.pricingSectionSubheader}
                    plans={pricingSectionRecord.plans}
                  />
                );
              default:
                return (
                  <Pricing
                    key={pricingSectionRecord.id}
                    header={pricingSectionRecord.pricingSectionHeader}
                    subheader={pricingSectionRecord.pricingSectionSubheader}
                    plans={pricingSectionRecord.plans}
                  />
                );
            }

          case "featured_posts_section":
            const featuredPostsSectionRecord =
              section as FeaturedPostsSectionRecord;
            switch (featuredPostsSectionRecord.displayOptions) {
              case "modern_cards":
                return (
                  <ModernPostCards
                    key={featuredPostsSectionRecord.id}
                    locale={locale}
                    blogData={featuredPostsSectionRecord.featuredPosts}
                    blogHeader={featuredPostsSectionRecord.featuredPostsHeader}
                    blogSubheader={
                      featuredPostsSectionRecord.featuredPostsSubheader
                    }
                  />
                );
              case "carrousel":
                return (
                  <CarrouselFeaturedPosts
                    key={featuredPostsSectionRecord.id}
                    locale={locale}
                    blogData={featuredPostsSectionRecord.featuredPosts}
                    blogHeader={featuredPostsSectionRecord.featuredPostsHeader}
                    blogSubheader={
                      featuredPostsSectionRecord.featuredPostsSubheader
                    }
                  />
                );
              case "minimalist_grid":
                return (
                  <MinimalistFeaturedPostsGrid
                    key={featuredPostsSectionRecord.id}
                    locale={locale}
                    blogData={featuredPostsSectionRecord.featuredPosts}
                    blogHeader={featuredPostsSectionRecord.featuredPostsHeader}
                    blogSubheader={
                      featuredPostsSectionRecord.featuredPostsSubheader
                    }
                  />
                );
              case "full_image_card":
                return (
                  <FullImageFeaturedPosts
                    key={featuredPostsSectionRecord.id}
                    locale={locale}
                    blogData={featuredPostsSectionRecord.featuredPosts}
                    blogHeader={featuredPostsSectionRecord.featuredPostsHeader}
                    blogSubheader={
                      featuredPostsSectionRecord.featuredPostsSubheader
                    }
                  />
                );
              default:
                return (
                  <Blog
                    key={featuredPostsSectionRecord.id}
                    locale={locale}
                    blogData={featuredPostsSectionRecord.featuredPosts}
                    blogHeader={featuredPostsSectionRecord.featuredPostsHeader}
                    blogSubheader={
                      featuredPostsSectionRecord.featuredPostsSubheader
                    }
                  />
                );
            }

          case "team_section":
            const teamSectionRecord = section as TeamSectionRecord;
            if (teamSectionRecord.displayOptions === "compact")
              return (
                <CompactTeam
                  key={teamSectionRecord.id}
                  header={teamSectionRecord.title}
                  subheader={teamSectionRecord.subtitle}
                  members={teamSectionRecord.showcasedMembers}
                  lng={locale}
                />
              );
            return (
              <ExpandedTeam
                key={teamSectionRecord.id}
                header={teamSectionRecord.title}
                subheader={teamSectionRecord.subtitle}
                members={teamSectionRecord.showcasedMembers}
                lng={locale}
              />
            );
          case "faq_section":
            const faqSectionRecord = section as FaqSectionRecord;
            if (faqSectionRecord.displayOptions === "accordion")
              return (
                <FAQAccordion
                  key={faqSectionRecord.id}
                  title={faqSectionRecord.title}
                  subtitle={faqSectionRecord.subtitle}
                  questions={faqSectionRecord.questions}
                />
              );
            return (
              <FAQGrid
                key={faqSectionRecord.id}
                title={faqSectionRecord.title}
                subtitle={faqSectionRecord.subtitle}
                questions={faqSectionRecord.questions}
              />
            );
          case "stats_section":
            const statsSectionRecord = section as StatsSectionRecord;
            return (
              <StatsSection
                key={statsSectionRecord.id}
                title={statsSectionRecord.title}
                subtitle={statsSectionRecord.subtitle}
                statistic={statsSectionRecord.statistic}
              />
            );
          case "stat_section":
            const statSectionRecord = section as StatSectionRecord;
            return (
              <StatSection
                key={"stat" + statSectionRecord.id}
                title={statSectionRecord.title}
                subtitle={statSectionRecord.subtitle}
                button={statSectionRecord.button}
                statItem={statSectionRecord.statItem}
              />
            );
          case "about_intro":
            const aboutIntroSectionRecord = section as AboutIntroRecord;
            return (
              <AboutIntro
                key={aboutIntroSectionRecord.id}
                header={aboutIntroSectionRecord.header}
                subheader={aboutIntroSectionRecord.subheader}
                introduction={aboutIntroSectionRecord.introductionText}
                images={aboutIntroSectionRecord.images}
                preHeader={aboutIntroSectionRecord.preHeader}
              />
            );
          case "all_posts_section":
            const allPostsSectionRecord = section as AllPostsSectionRecord;
            return (
              <PostGridRenderer
                key={`post-grid-renderer-${index}`}
                data={posts}
                lng={locale}
                postMeta={postMeta}
              />
            );
          case "redirect_section":
            const redirectSectionRecord = section as RedirectSectionRecord;
            redirect(`/${locale}/${redirectSectionRecord.slugToRedirectTo}`);
          default:
            return <></>;
        }
      })}
    </>
  );
}
