import React from "react";
import { gfsDidot } from "@/components/Common/SectionTitle";
import { ReactMarkdown } from "react-markdown/lib/react-markdown";
import Link from "next/link";
type Props = {
  adresseLabel: string;
  adresseValue: string;
  contactLabel: string;
  phoneValue: string;
  emailValue: string;
  horaireLabel: string;
  rdvLabel: string;
  horaireValue: string;
};
const ContactSection = ({
  adresseLabel,
  adresseValue,
  contactLabel,
  phoneValue,
  emailValue,
  horaireLabel,
  rdvLabel,
  horaireValue,
}: Props) => {
  return (
    <div className="bg-primary py-12 text-white">
      <div className="container">
        <div className="flex flex-col gap-12 lg:flex-row lg:justify-center lg:gap-32">
          <div>
            <p
              className={`uppercase ${gfsDidot.className} text-center text-2xl text-activeNav lg:text-start`}
            >
              {horaireLabel}
            </p>
            <div className="mx-auto max-w-[250px] lg:mx-0">
              <p
                className={`pb-2 text-center text-base text-activeNav lg:text-start`}
              >
                {rdvLabel}
              </p>
              <div
                className={`prose text-center prose-code:font-body prose-code:text-xl prose-pre:bg-transparent prose-pre:p-0 lg:text-start`}
              >
                <ReactMarkdown>{horaireValue}</ReactMarkdown>
              </div>
            </div>
          </div>
          <div className="mx-auto max-w-[380px] lg:mx-0">
            <div className="pb-8">
              <p
                className={`text-center uppercase text-activeNav ${gfsDidot.className} pb-2 text-2xl`}
              >
                {adresseLabel}
              </p>
              <a
                className={`block text-center text-xl`}
                href="https://maps.app.goo.gl/By7XXQ8QjX3Pn5t66"
                target="_blank"
              >
                {adresseValue}
              </a>
            </div>
            <div className="border-t-2 border-[#F3ECE7] pt-8">
              <p
                className={`text-center uppercase text-activeNav ${gfsDidot.className} pb-2 text-2xl`}
              >
                {contactLabel}
              </p>
              <a
                href={`tel:${phoneValue}`}
                className={`block text-center text-xl`}
                target="_blank"
              >
                {phoneValue}
              </a>
              <p className={` text-center text-xl`}>{emailValue}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactSection;
