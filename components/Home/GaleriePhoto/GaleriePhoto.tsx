"use client";
import React from "react";
import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import "swiper/css/navigation";
import Swiper3d from "./Swiper3d";
import { ButtonRecord, FileField } from "@/graphql/types/graphql";
import { Maybe } from "graphql/jsutils/Maybe";
import { gfsDidot } from "@/components/Common/SectionTitle";
import ReactMarkdown from "react-markdown";
import { Image as DatoImage } from "react-datocms";
import Link from "next/link";

type Props = {
  title: string;
  images: Maybe<FileField>[] | undefined;
  button: ButtonRecord;
};

const GaleriePhoto = ({ title, images, button }: Props) => {
  const params = {
    navigation: true,
    pagination: true,
    effectCoverflow: true,
    effect: "coverflow",
    grabCursor: true,
    centeredSlides: true,
    loop: true,
    coverflowEffect: {
      rotate: 0,
      stretch: 0,
      depth: 100,
      modifier: 2.5,
    },
    breakpoints: {
      640: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 2,
      },
    },
    injectStyles: [
      `
              .swiper-button-next,
              .swiper-button-prev {
                  display:none;
                  background-size: contain;
                  background-position: center;
                  background-repeat:no-repeat;
                  width:20px;
                  height:34px;
                  background-image: url('./arrow.png');
                  color:#151515;
                  box-shadow: 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1);
                  position: absolute;
                  top: calc(50% - 10px);
                  z-index: 4;
                  background:white;
                  align-items:center;
                  justify-content:center;
                  border-radius:100%;
                  border: 2px solid rgb(243 244 246 / var(--tw-bg-opacity));
                }
                .swiper-button-next:hover,
              .swiper-button-prev:hover{
                background:rgb(243 244 246 / var(--tw-bg-opacity));
              }
                .swiper-button-next{
                  right:10px;
                }
                .swiper-button-prev{
                  left:10px;
                }
                .swiper-button-next::after,
                .swiper-button-prev::after {
                  content: "";
                }
                .swiper-pagination {
                  display:flex;
                  gap:2px;
                  justify-content:center;
                  align-items:center;
                  position: absolute;
                  bottom: 15px;
                  width: 100%;
                  z-index: 5;
                }
                .swiper-pagination-bullet{
                  display:block;
                  width: 10px;
                  height: 10px;
                  border:2px solid white;
                  border-radius: 100%;
                  background:transparent;
                }
                .swiper-pagination-bullet-active{
                  background-color: #73503A;
                }
                @media (min-width: 768px) {
                  .swiper-button-next,
                  .swiper-button-prev {
                    display:flex;
                    padding: 8px 16px;
                  }
                }
            `,
    ],
  };
  return (
    <section className="bg-white py-12 text-primary">
      <div className="container">
        <h2
          className={`${gfsDidot.className} prose mb-12 text-center text-4xl text-primary prose-strong:font-normal prose-strong:text-tertiary md:mb-12 md:text-6xl lg:mx-auto lg:max-w-2xl lg:uppercase `}
        >
          <ReactMarkdown>{title || ""}</ReactMarkdown>
        </h2>
        <div className="container">
          <Swiper3d
            items={images}
            resourceName="images"
            itemComponent={GaleriePhotoItem}
            params={params}
          />
          <div className="flex justify-center">
            <Link
              href={button?.url!}
              className="group inline-block text-base font-semibold"
            >
              <div className="group relative flex items-center gap-1 overflow-hidden rounded-full border-2 border-primary bg-white px-16 py-3 text-white duration-300 after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:z-0 after:bg-primary group-hover:text-primary group-hover:after:top-full after:motion-safe:duration-300">
                <span className="relative z-[1] motion-safe:duration-100">
                  {button.label}
                </span>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default GaleriePhoto;

function GaleriePhotoItem({ images }: { images: any }) {
  const { responsiveImage } = images;
  return (
    <div className="relative h-[380px] max-w-[550px] overflow-hidden rounded-lg border-4 border-primary">
      <DatoImage
        className="h-full w-full"
        objectFit="cover"
        data={responsiveImage}
      />
    </div>
  );
}
