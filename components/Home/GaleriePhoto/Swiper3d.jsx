import React, { useEffect, useRef } from "react";
import { SwiperContainer } from "swiper/element/bundle";
import { register } from "swiper/element";

const Swiper3d = ({
  items,
  resourceName,
  itemComponent: ItemComponent,
  params,
}) => {
  const swiperRef = useRef(null);
  useEffect(() => {
    register();
    const swiperContainer = swiperRef.current;
    Object.assign(swiperContainer, params);
    swiperContainer.initialize();
  }, []);

  return (
    <swiper-container
      ref={swiperRef}
      init="false"
      class="realtive mb-12 px-0 lg:px-16"
    >
      {items.map((item, index) => (
        <swiper-slide key={index}>
          <ItemComponent {...{ [resourceName]: item }} />
        </swiper-slide>
      ))}
    </swiper-container>
  );
};

export default Swiper3d;
