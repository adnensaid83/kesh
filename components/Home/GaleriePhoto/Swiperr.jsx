import React, { useRef } from "react";
import { Image as DatoImage } from "react-datocms";
import { register } from "swiper/element/bundle";
register();

const Swiperr = ({ images }) => {
  const swiperRef = useRef(null);
  return (
    <swiper-container init="false">
      {images.map((image, index) => {
        return (
          <swiper-slide ref={swiperRef} key={index}>
            <DatoImage
              className="h-36 w-36"
              objectFit="cover"
              data={image?.responsiveImage}
            />
          </swiper-slide>
        );
      })}
    </swiper-container>
  );
};

export default Swiperr;
