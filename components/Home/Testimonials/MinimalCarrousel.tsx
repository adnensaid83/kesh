"use client";

import { TestimonialRecord } from "@/graphql/types/graphql";
import { Maybe } from "graphql/jsutils/Maybe";
import { StructuredText as StructuredTextField } from "react-datocms/structured-text";
import { Image as DatoImage } from "react-datocms";
import { useEffect, useState, useRef } from "react";
import Highlighter from "@/components/Common/Highlighter";
import { Record, StructuredText } from "datocms-structured-text-utils";
import { gfsDidot } from "@/components/Common/SectionTitle";
import ReactMarkdown from "react-markdown";
import "swiper/css/bundle";
import {
  Swiper,
  SwiperClass,
  SwiperRef,
  SwiperSlide,
  useSwiper,
} from "swiper/react";
import "swiper/css";
import "swiper/css/effect-cards";
import { Autoplay, EffectCards } from "swiper/modules";
import { register } from "swiper/element/bundle";
import Link from "next/link";
const primaryColor = "#C39788";
type Props = {
  reviews: TestimonialRecord[];
  header: string;
  subheader: Maybe<string>;
};
type ReviewDataProps = {
  author_name: string;
  rating: number;
  text: string;
};
const MinimalCarrousel = ({ reviews, header, subheader }: Props) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const [placeDetails, setPlaceDetails] = useState(null);
  const [reviewsData, setReviewsData] = useState<ReviewDataProps[]>([]);
  const [totalReviews, setTotalReviews] = useState<number | null>(null);
  const [averageRating, setAverageRating] = useState<number | null>(null);
  const [googleReviewsLink, setGoogleReviewsLink] = useState<string | null>(
    null
  );
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const place_id = "ChIJtS-aPUrF9EcRxRMf3jhkjeQ";
    async function fetchReviews() {
      try {
        const response = await fetch("/api/reviews?place_id=" + place_id);
        if (!response.ok) {
          throw new Error("Failed to fetch reviews");
        }
        const data = await response.json();
        setReviewsData(data.result.reviews);
        setTotalReviews(data.result.user_ratings_total || 0);
        setAverageRating(data.result.rating || 0);
        setGoogleReviewsLink(`https://g.page/r/CcUTH944ZI3kEBM/review`);
      } catch (err: any) {
        setError(err.message);
      } finally {
        setLoading(false);
      }
    }
    fetchReviews();
  }, []);

  const handleNext = () => {
    setCurrentIndex((prevIndex) => (prevIndex + 1) % reviews.length);
  };

  const handlePrev = () => {
    setCurrentIndex((prevIndex) =>
      prevIndex === 0 ? reviews.length - 1 : prevIndex - 1
    );
  };

  const currentReview = reviews[currentIndex];
  const progressCircle = useRef<SVGSVGElement | null>(null);

  const progressContent = useRef<HTMLSpanElement | null>(null);
  const onAutoplayTimeLeft = (s: any, time: number, progress: number): void => {
    const swiper = s;
    progressContent.current!.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" className="bi bi-arrow-right" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"/>
</svg>`;
    progressContentPrev.current!.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" className="bi bi-arrow-left" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8"/>
</svg>`;

    if (swiper.isEnd) {
      progressCirclePrev.current!.style.setProperty(
        "--progress",
        `${1 - progress}`
      );
      //progressContent.current!.textContent = `${Math.ceil(time / 1000)}`;
      progressCircle.current!.style.setProperty("--progress", "0");
    } else {
      progressCircle.current!.style.setProperty(
        "--progress",
        `${1 - progress}`
      );
      progressCirclePrev.current!.style.setProperty("--progress", "0");
    }
  };
  const progressCirclePrev = useRef<SVGSVGElement | null>(null);

  const progressContentPrev = useRef<HTMLSpanElement | null>(null);

  function SlidePrevButton() {
    const swiper = useSwiper();

    return (
      <button onClick={() => swiper.slidePrev()}>
        <div
          className="absolute bottom-[48px] right-[68px] z-10 flex h-[48px] w-[48px] items-center justify-center text-blue-50"
          slot="container-end"
        >
          <svg
            viewBox="0 0 48 48"
            ref={progressCirclePrev}
            className="absolute left-0 top-0 z-10 h-full w-full rotate-[-90deg] rounded-full"
          >
            <circle
              cx="24"
              cy="24"
              r="20"
              style={{
                strokeWidth: 4,
                stroke: "#a27051bf",
                fill: "none",
              }}
            ></circle>
            <circle
              cx="24"
              cy="24"
              r="20"
              style={{
                strokeWidth: 4,
                strokeDasharray: 125.6,
                strokeDashoffset: `calc(125.6px * (1 - var(--progress)))`,
                stroke: "#73503A",
                fill: "none",
              }}
            ></circle>
          </svg>
          <span ref={progressContentPrev} className="text-tertiary"></span>
        </div>
      </button>
    );
  }
  function SlideNextButton() {
    const swiper = useSwiper();
    return (
      <button
        onClick={() => swiper.slideNext()}
        className="absolute bottom-[48px] right-[16px] z-10 flex h-[48px] w-[48px] items-center justify-center text-tertiary"
        slot="container-end"
      >
        <svg
          viewBox="0 0 48 48"
          ref={progressCircle}
          className="absolute left-0 top-0 z-10 h-full w-full rotate-[-90deg] rounded-full"
        >
          <circle
            cx="24"
            cy="24"
            r="20"
            style={{
              strokeWidth: 4,
              stroke: "#a27051bf",
              fill: "none",
            }}
          ></circle>
          <circle
            cx="24"
            cy="24"
            r="20"
            style={{
              strokeWidth: 4,
              strokeDasharray: 125.6,
              strokeDashoffset: `calc(125.6px * (1 - var(--progress)))`,
              stroke: "#73503A",
              fill: "none",
            }}
          ></circle>
        </svg>
        <span ref={progressContent} className="text-tertiary"></span>
      </button>
    );
  }
  if (loading) {
    return <p>Loading reviews...</p>;
  }

  if (error) {
    return <p>Error: {error}</p>;
  }

  return (
    <div className="my-12">
      <h2
        className={`${gfsDidot.className} prose mb-12 text-center text-4xl font-bold text-primary prose-strong:font-normal prose-strong:text-tertiary md:mb-12 md:text-6xl lg:mx-auto lg:max-w-2xl lg:uppercase `}
      >
        <ReactMarkdown>{header}</ReactMarkdown>
      </h2>
      <p className="text-center">
        Évaluation Google: {averageRating}/5 basée sur {totalReviews} avis{" "}
      </p>
      {reviewsData.length > 0 ? (
        <Swiper
          effect={"cards"}
          grabCursor={true}
          modules={[EffectCards, Autoplay]}
          onAutoplayTimeLeft={onAutoplayTimeLeft}
          autoplay={{
            delay: 10000,
            disableOnInteraction: false,
          }}
          className="mt-6 w-[220px] md:w-[540px] lg:w-[740px]"
        >
          {reviews.map((review) => (
            <SwiperSlide
              className="relative rounded-2xl border-4 border-black bg-white"
              key={review.id}
            >
              <SingleReview testimonial={review} />
              <div
                className={`absolute right-0 top-0 z-[-1] opacity-30 lg:opacity-100`}
              >
                <svg
                  width="450"
                  height="556"
                  viewBox="0 0 450 556"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  className={`rotate-90 md:rotate-0 `}
                >
                  <circle
                    cx="277"
                    cy="63"
                    r="225"
                    fill="url(#paint0_linear_25:217)"
                  />
                  <circle
                    cx="17.9997"
                    cy="182"
                    r="18"
                    fill="url(#paint1_radial_25:217)"
                  />
                  <circle
                    cx="76.9997"
                    cy="288"
                    r="34"
                    fill="url(#paint2_radial_25:217)"
                  />
                  <circle
                    cx="325.486"
                    cy="302.87"
                    r="180"
                    transform="rotate(-37.6852 325.486 302.87)"
                    fill="url(#paint3_linear_25:217)"
                  />
                  <circle
                    opacity="0.8"
                    cx="184.521"
                    cy="315.521"
                    r="132.862"
                    transform="rotate(114.874 184.521 315.521)"
                    stroke="url(#paint4_linear_25:217)"
                  />
                  <circle
                    opacity="0.8"
                    cx="356"
                    cy="290"
                    r="179.5"
                    transform="rotate(-30 356 290)"
                    stroke="url(#paint5_linear_25:217)"
                  />
                  <circle
                    opacity="0.8"
                    cx="191.659"
                    cy="302.659"
                    r="133.362"
                    transform="rotate(133.319 191.659 302.659)"
                    fill="url(#paint6_linear_25:217)"
                  />
                  <defs>
                    <linearGradient
                      id="paint0_linear_25:217"
                      x1="-54.5003"
                      y1="-178"
                      x2="222"
                      y2="288"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stopColor={primaryColor} />
                      <stop
                        offset="1"
                        stopColor={primaryColor}
                        stopOpacity="0"
                      />
                    </linearGradient>
                    <radialGradient
                      id="paint1_radial_25:217"
                      cx="0"
                      cy="0"
                      r="1"
                      gradientUnits="userSpaceOnUse"
                      gradientTransform="translate(17.9997 182) rotate(90) scale(18)"
                    >
                      <stop
                        offset="0.145833"
                        stopColor={primaryColor}
                        stopOpacity="0"
                      />
                      <stop
                        offset="1"
                        stopColor={primaryColor}
                        stopOpacity="0.08"
                      />
                    </radialGradient>
                    <radialGradient
                      id="paint2_radial_25:217"
                      cx="0"
                      cy="0"
                      r="1"
                      gradientUnits="userSpaceOnUse"
                      gradientTransform="translate(76.9997 288) rotate(90) scale(34)"
                    >
                      <stop
                        offset="0.145833"
                        stopColor={primaryColor}
                        stopOpacity="0"
                      />
                      <stop
                        offset="1"
                        stopColor={primaryColor}
                        stopOpacity="0.08"
                      />
                    </radialGradient>
                    <linearGradient
                      id="paint3_linear_25:217"
                      x1="226.775"
                      y1="-66.1548"
                      x2="292.157"
                      y2="351.421"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stopColor={primaryColor} />
                      <stop
                        offset="1"
                        stopColor={primaryColor}
                        stopOpacity="0"
                      />
                    </linearGradient>
                    <linearGradient
                      id="paint4_linear_25:217"
                      x1="184.521"
                      y1="182.159"
                      x2="184.521"
                      y2="448.882"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stopColor={primaryColor} />
                      <stop offset="1" stopColor="white" stopOpacity="0" />
                    </linearGradient>
                    <linearGradient
                      id="paint5_linear_25:217"
                      x1="356"
                      y1="110"
                      x2="356"
                      y2="470"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stopColor={primaryColor} />
                      <stop offset="1" stopColor="white" stopOpacity="0" />
                    </linearGradient>
                    <linearGradient
                      id="paint6_linear_25:217"
                      x1="118.524"
                      y1="29.2497"
                      x2="166.965"
                      y2="338.63"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stopColor={primaryColor} />
                      <stop
                        offset="1"
                        stopColor={primaryColor}
                        stopOpacity="0"
                      />
                    </linearGradient>
                  </defs>
                </svg>
              </div>
              <div
                className={`absolute bottom-0 left-0 z-[-1] opacity-30 lg:opacity-100`}
              >
                <svg
                  width="364"
                  height="201"
                  viewBox="0 0 364 201"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M5.88928 72.3303C33.6599 66.4798 101.397 64.9086 150.178 105.427C211.155 156.076 229.59 162.093 264.333 166.607C299.076 171.12 337.718 183.657 362.889 212.24"
                    stroke="url(#paint0_linear_25:218)"
                  />
                  <path
                    d="M-22.1107 72.3303C5.65989 66.4798 73.3965 64.9086 122.178 105.427C183.155 156.076 201.59 162.093 236.333 166.607C271.076 171.12 309.718 183.657 334.889 212.24"
                    stroke="url(#paint1_linear_25:218)"
                  />
                  <path
                    d="M-53.1107 72.3303C-25.3401 66.4798 42.3965 64.9086 91.1783 105.427C152.155 156.076 170.59 162.093 205.333 166.607C240.076 171.12 278.718 183.657 303.889 212.24"
                    stroke="url(#paint2_linear_25:218)"
                  />
                  <path
                    d="M-98.1618 65.0889C-68.1416 60.0601 4.73364 60.4882 56.0734 102.431C120.248 154.86 139.905 161.419 177.137 166.956C214.37 172.493 255.575 186.165 281.856 215.481"
                    stroke="url(#paint3_linear_25:218)"
                  />
                  <circle
                    opacity="0.8"
                    cx="214.505"
                    cy="60.5054"
                    r="49.7205"
                    transform="rotate(-13.421 214.505 60.5054)"
                    stroke="url(#paint4_linear_25:218)"
                  />
                  <circle
                    cx="220"
                    cy="63"
                    r="43"
                    fill="url(#paint5_radial_25:218)"
                  />
                  <defs>
                    <linearGradient
                      id="paint0_linear_25:218"
                      x1="184.389"
                      y1="69.2405"
                      x2="184.389"
                      y2="212.24"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stopColor={primaryColor} stopOpacity="0" />
                      <stop offset="1" stopColor={primaryColor} />
                    </linearGradient>
                    <linearGradient
                      id="paint1_linear_25:218"
                      x1="156.389"
                      y1="69.2405"
                      x2="156.389"
                      y2="212.24"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stopColor={primaryColor} stopOpacity="0" />
                      <stop offset="1" stopColor={primaryColor} />
                    </linearGradient>
                    <linearGradient
                      id="paint2_linear_25:218"
                      x1="125.389"
                      y1="69.2405"
                      x2="125.389"
                      y2="212.24"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stopColor={primaryColor} stopOpacity="0" />
                      <stop offset="1" stopColor={primaryColor} />
                    </linearGradient>
                    <linearGradient
                      id="paint3_linear_25:218"
                      x1="93.8507"
                      y1="67.2674"
                      x2="89.9278"
                      y2="210.214"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stopColor={primaryColor} stopOpacity="0" />
                      <stop offset="1" stopColor={primaryColor} />
                    </linearGradient>
                    <linearGradient
                      id="paint4_linear_25:218"
                      x1="214.505"
                      y1="10.2849"
                      x2="212.684"
                      y2="99.5816"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stopColor={primaryColor} />
                      <stop
                        offset="1"
                        stopColor={primaryColor}
                        stopOpacity="0"
                      />
                    </linearGradient>
                    <radialGradient
                      id="paint5_radial_25:218"
                      cx="0"
                      cy="0"
                      r="1"
                      gradientUnits="userSpaceOnUse"
                      gradientTransform="translate(220 63) rotate(90) scale(43)"
                    >
                      <stop
                        offset="0.145833"
                        stopColor="white"
                        stopOpacity="0"
                      />
                      <stop offset="1" stopColor="white" stopOpacity="0.08" />
                    </radialGradient>
                  </defs>
                </svg>
              </div>
            </SwiperSlide>
          ))}
          <SlideNextButton />
          <SlidePrevButton />
        </Swiper>
      ) : (
        <p>No reviews available.</p>
      )}
      {googleReviewsLink && (
        <>
          <div className="flex justify-center">
            <Link
              href={googleReviewsLink}
              className="group inline-block text-base font-semibold"
              target="_blank"
              rel="noopener noreferrer"
            >
              <div className="group relative flex items-center gap-1 overflow-hidden rounded-full border-2 border-primary bg-white px-16 py-3 text-white duration-300 after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:z-0 after:bg-primary group-hover:text-primary group-hover:after:top-full after:motion-safe:duration-300">
                <span className="relative z-[1] motion-safe:duration-100">
                  Voir tous les avis Google
                </span>
              </div>
            </Link>
          </div>
        </>
      )}
    </div>
  );
};

export default MinimalCarrousel;
const starIcon = (
  <svg width="18" height="16" viewBox="0 0 18 16" className="fill-current">
    <path d="M9.09815 0.361679L11.1054 6.06601H17.601L12.3459 9.59149L14.3532 15.2958L9.09815 11.7703L3.84309 15.2958L5.85035 9.59149L0.595291 6.06601H7.0909L9.09815 0.361679Z" />
  </svg>
);

const SingleReview = ({ testimonial }: { testimonial: TestimonialRecord }) => {
  const { rating, reviewerName, reviewerPicture, review, reviewerTitle } =
    testimonial;

  let ratingIcons = [];
  for (let index = 0; index < rating; index++) {
    ratingIcons.push(
      <span key={index} className="text-yellow">
        {starIcon}
      </span>
    );
  }

  return (
    <div className="h-96 w-full last:block md:last:hidden lg:last:block">
      <div className=" mx-auto flex h-full flex-col justify-center rounded-md px-4 shadow-one dark:bg-[#1D2144] md:items-center lg:items-start lg:px-5 xl:px-8">
        <div className="mb-12 h-36 border-b border-body-color border-opacity-10 pb-8 text-sm leading-relaxed text-black dark:border-white dark:border-opacity-10 dark:text-white md:text-lg">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="48"
            height="48"
            fill="#FBB040"
            className="bi bi-quote"
            viewBox="0 0 16 16"
          >
            <path d="M12 12a1 1 0 0 0 1-1V8.558a1 1 0 0 0-1-1h-1.388q0-.527.062-1.054.093-.558.31-.992t.559-.683q.34-.279.868-.279V3q-.868 0-1.52.372a3.3 3.3 0 0 0-1.085.992 4.9 4.9 0 0 0-.62 1.458A7.7 7.7 0 0 0 9 7.558V11a1 1 0 0 0 1 1zm-6 0a1 1 0 0 0 1-1V8.558a1 1 0 0 0-1-1H4.612q0-.527.062-1.054.094-.558.31-.992.217-.434.559-.683.34-.279.868-.279V3q-.868 0-1.52.372a3.3 3.3 0 0 0-1.085.992 4.9 4.9 0 0 0-.62 1.458A7.7 7.7 0 0 0 3 7.558V11a1 1 0 0 0 1 1z" />
          </svg>
          <p className="line-clamp-4">
            <StructuredTextField
              data={review.value as StructuredText<Record, Record>}
              renderNode={Highlighter}
            />
          </p>
        </div>
        <div className="flex items-center gap-1 md:w-full md:px-4 lg:px-0">
          <div className="relative flex h-[50px] w-[50px] overflow-hidden rounded-full lg:mr-4 lg:h-[50px] lg:w-full lg:max-w-[50px]">
            <DatoImage
              data={reviewerPicture.responsiveImage}
              className="h-full w-full object-contain"
              layout="fill"
              objectFit="cover"
              objectPosition="50% 50%"
            />
          </div>
          <div className="">
            <p className="mb-1 text-base font-semibold text-black dark:text-white lg:text-base xl:text-lg">
              {reviewerName}
            </p>
            <div className="flex items-center space-x-1">{ratingIcons}</div>
            {/* <p className="text-sm text-black">{reviewerTitle}</p> */}
          </div>
        </div>
      </div>
    </div>
  );
};
