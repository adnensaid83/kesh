import { gfsDidot } from "@/components/Common/SectionTitle";
import {
  ButtonRecord,
  FileField,
  ServiceRecord,
} from "@/graphql/types/graphql";
import { Maybe } from "graphql/jsutils/Maybe";
import Link from "next/link";
import React from "react";
import { Image as DatoImage } from "react-datocms";
import { ReactMarkdown } from "react-markdown/lib/react-markdown";
type Props = {
  title: string;
  service: ServiceRecord[];
};

const ServiceSection = ({ title, service }: Props) => {
  return (
    <div className="bg-white pt-12 text-primary">
      <div className="mx-auto max-w-[900px]">
        <h2
          className={`${gfsDidot.className} prose mb-12 text-center text-4xl text-primary prose-strong:font-normal prose-strong:text-tertiary md:mb-12 md:text-6xl lg:mx-auto lg:max-w-2xl lg:uppercase `}
        >
          <ReactMarkdown>{title || ""}</ReactMarkdown>
        </h2>
        <div className="flex flex-col gap-4 md:flex-row">
          {service.map((s, i) => {
            return (
              <div
                key={i}
                className="mx-8 flex-1 rounded-xl bg-secondary py-4 text-primary md:mx-0"
              >
                <h3
                  className={`${gfsDidot.className} text-center text-3xl uppercase`}
                >
                  Kesh
                </h3>
                <p className="mb-4 text-center text-xs uppercase">Réception</p>
                <div className="relative flex flex-col items-center justify-center gap-4 pb-12">
                  {s.image?.responsiveImage && (
                    <DatoImage
                      className="h-72"
                      objectFit="cover"
                      data={s.image?.responsiveImage}
                    />
                  )}
                  <h4 className="text-center text-2xl">{s.bigtitle}</h4>
                  <p
                    className={`${gfsDidot.className} px-16 text-center text-3xl uppercase`}
                  >
                    {s.content}
                  </p>
                  <div className="absolute top-4  h-[95%] w-[95%] border-4 border-primary"></div>
                </div>
                <div className="flex justify-center">
                  <Link
                    href={s.button?.url!}
                    className="group inline-block text-base font-semibold"
                  >
                    <div className="group relative flex items-center gap-1 overflow-hidden rounded-full border-2 border-primary bg-white px-16 py-3 text-white duration-300 after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:z-0 after:bg-primary group-hover:text-primary group-hover:after:top-full after:motion-safe:duration-300">
                      <span className="relative z-[1] motion-safe:duration-100">
                        {s.button?.label}
                      </span>
                    </div>
                  </Link>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ServiceSection;
