import SectionTitle, { gfsDidot } from "../../Common/SectionTitle";

import { Maybe } from "graphql/jsutils/Maybe";
import { ImageFileField } from "@/graphql/types/graphql";
import { ReactMarkdown } from "react-markdown/lib/react-markdown";
import Link from "next/link";

type Props = {
  videoHeader: string;
  videoSubheader: Maybe<string>;
  videoUid: string;
  videoThumbnail: ImageFileField;
  videoProvider: string;
};

const Video = ({ videoHeader, videoSubheader }: Props) => {
  return (
    <section className="relative z-10 overflow-hidden">
      <div className=" mt-[75px] h-auto w-[540px] md:h-auto md:w-screen first-letter:lg:mt-0 xl:mt-0 xl:h-screen ">
        <video autoPlay width="100%" height="100%" loop muted preload="auto">
          <source src="/Keshvideo.mp4" type="video/mp4" />
        </video>
      </div>
      <div className="absolute bottom-0 left-0 right-0 top-0 flex flex-col items-center justify-center ">
        <div
          className={`flex h-[300px] w-[300px] animate-morphAnimation flex-col items-center justify-center rounded-full border-[4px] border-primary bg-primary bg-opacity-60 p-2 text-center md:h-[340px] md:w-[340px] lg:h-[500px] lg:w-[500px] lg:p-12`}
        >
          <h2
            className={`${gfsDidot.className} mb-8 hidden text-xl font-bold text-white dark:text-white md:text-[70px] lg:block`}
          >
            {videoHeader}
          </h2>
          <div className="text-lg !leading-relaxed text-white md:text-2xl lg:mb-6 lg:text-3xl">
            <ReactMarkdown>{videoSubheader || ""}</ReactMarkdown>
          </div>
          <Link
            href={"/contact"}
            className="group hidden text-base font-semibold text-primary lg:block"
          >
            <div className="group relative flex items-center gap-1 overflow-hidden rounded-full border-2 border-primary bg-primary px-16 py-3 text-primary duration-300 after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:z-0 after:bg-white group-hover:text-white group-hover:after:top-full after:motion-safe:duration-300">
              <span className="relative z-[1] motion-safe:duration-100">
                Contactez-nous
              </span>
            </div>
          </Link>
        </div>
      </div>
    </section>
  );
};

export default Video;

{
  /*
              <div className="hidden items-center space-x-3 lg:flex xl:absolute xl:right-6 xl:space-x-6">
              <a title="Contattaci" className="group" href="/contact">
                <div className="group relative flex items-center gap-1 overflow-hidden rounded-full border border-primary bg-primary px-6 py-2 text-primary duration-300 after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:z-0 after:bg-white group-hover:text-white group-hover:after:top-full after:motion-safe:duration-300">
                  <span className="relative z-[1] motion-safe:duration-100">
                    Contactez-nous
                  </span>
                </div>
              </a>
            </div>
  
  <ModalVideo
        channel="youtube"
        isOpen={isOpen}
        youtube={{
          mute: 1,
          autoplay: 1,
          loop: 1,
          playlist: `${videoUid}, ${videoUid}`,
          rel: 0,
          iv_load_policy: 0,
          fs: 1,
          disablekb: 1,
          controls: 0,
        }}
        videoId={videoUid}
        onClose={() => setOpen(false)}
      /> 

      <div className="absolute bottom-0 left-0 right-0 z-[-1]">
        <Image
          fill
          src="/images/video/shape.svg"
          alt="shape"
          className="w-full"
        />
      </div>

      */
}

{
  /*         <div className="-mx-4 flex flex-wrap">
          <div className="w-full px-4">
            <div className="mx-auto max-w-full overflow-hidden">
              <div className="relative aspect-[77/40] items-center justify-center">
                
                <DatoImage
                  className="h-full w-full object-cover"
                  layout="fill"
                  objectFit="cover"
                  objectPosition="top"
                  data={videoThumbnail.responsiveImage}
                />
                                <div className="absolute right-0 top-0 flex h-full w-full items-center justify-center">
                  <button
                    onClick={() => setOpen(true)}
                    className="flex h-[70px] w-[70px] items-center justify-center rounded-full bg-white bg-opacity-75 text-primary transition hover:bg-opacity-100"
                  >
                    <svg
                      width="16"
                      height="18"
                      viewBox="0 0 16 18"
                      className="fill-current"
                    >
                      <path d="M15.5 8.13397C16.1667 8.51888 16.1667 9.48112 15.5 9.86602L2 17.6603C1.33333 18.0452 0.499999 17.564 0.499999 16.7942L0.5 1.20577C0.5 0.43597 1.33333 -0.0451549 2 0.339745L15.5 8.13397Z" />
                    </svg>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div> */
}
{
  /* <ReactPlayer
            className=""
            url={`https://www.youtube.com/watch?v=${videoUid}`}
            width={"100%"}
            height={"100%"}
            playing={true}
            loop={true}
            muted={true}
            volume={1}
            controls={false}
          /> */
}
