import React from "react";
import { Maybe } from "graphql/jsutils/Maybe";
import { FileField } from "@/graphql/types/graphql";
import { gfsDidot } from "@/components/Common/SectionTitle";
import { Image as DatoImage } from "react-datocms";
import { ReactMarkdown } from "react-markdown/lib/react-markdown";

type Props = {
  title: string;
  content: string;
  smallTitle: string;
  prestationImage: Maybe<FileField> | undefined;
};

const PrestationSection = ({
  title,
  content,
  smallTitle,
  prestationImage,
}: Props) => {
  return (
    <div className="flex flex-col items-center pb-12 text-primary lg:pb-0">
      <h2
        className={`${gfsDidot.className} prose mb-12 text-center text-4xl font-bold text-primary prose-strong:font-normal prose-strong:text-tertiary md:mb-12 md:text-6xl lg:mx-auto lg:max-w-2xl lg:uppercase `}
      >
        <ReactMarkdown>{title}</ReactMarkdown>
      </h2>
      <div className="flex lg:hidden">
        <div className="container">
          <div className="flex flex-col border-4 border-primary">
            <div className="flex flex-1 flex-col border-b-4 border-primary">
              <div className=" flex-1">
                <p className="p-4 text-lg">{content}</p>
              </div>
            </div>
            <div className="">
              <div className=" flex flex-1 flex-col items-center justify-center bg-[rgba(15,15,15,.8)] text-white">
                {prestationImage?.responsiveImage && (
                  <DatoImage
                    className="h-72"
                    objectFit="cover"
                    data={prestationImage?.responsiveImage}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="relative hidden w-full items-center justify-center bg-cover bg-center object-cover lg:flex"
        style={{
          backgroundSize: "cover",
          backgroundImage: `url('${prestationImage?.responsiveImage?.src}')`,
        }}
      >
        <div className="container">
          <div className="my-12 flex flex-col border-4 border-[#F3ECE7] md:flex-row">
            <div className="flex flex-1 flex-col lg:border-r-4 lg:border-[#F3ECE7]">
              <div className="m-4 flex-1 bg-[rgba(243,236,231,.9)] text-primary">
                <div className="p-4 text-lg lg:p-12 xl:text-xl">
                  <ReactMarkdown>{content}</ReactMarkdown>
                </div>
              </div>
            </div>
            <div className="hidden lg:flex lg:flex-1 lg:flex-col">
              <div className="m-4 flex flex-1 flex-col items-center justify-center bg-[rgba(15,15,15,.8)] text-white">
                <p className={`text-6xl uppercase ${gfsDidot.className}`}>
                  {smallTitle}
                </p>
                <p className="mb-4 text-center text-xs uppercase">Réception</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PrestationSection;
