import { Maybe } from "graphql/jsutils/Maybe";
import { ReactMarkdown } from "react-markdown/lib/react-markdown";
import { GFS_Didot } from "next/font/google";
import Link from "next/link";

export const gfsDidot = GFS_Didot({
  subsets: ["greek"],
  weight: "400",
});

const SectionTitle = ({
  title,
  paragraph,
  width = "570px",
  center,
  mb = "0",
}: {
  title: string;
  paragraph: Maybe<string>;
  width?: string;
  center?: boolean;
  mb?: string;
}) => {
  return (
    <>
      <div
        className={`flex h-[500px] w-[500px] animate-morphAnimation flex-col items-center justify-center rounded-full border-[4px] border-primary ${
          center ? "mx-auto text-center" : ""
        } bg-primary bg-opacity-60 p-12 text-center`}
        style={{ maxWidth: width, marginBottom: mb }}
      >
        <h2
          className={`${gfsDidot.className} mb-8 hidden text-3xl font-bold text-white dark:text-white sm:text-4xl md:block md:text-[70px]`}
        >
          {title}
        </h2>
        <div className="mb-8 text-xl !leading-relaxed text-white md:text-3xl">
          <ReactMarkdown>{paragraph || ""}</ReactMarkdown>
        </div>
        <Link
          href={"/contact"}
          className="inline-block rounded-lg border-2 border-transparent bg-white px-12 py-3 text-center text-sm font-semibold text-primary outline-none ring-indigo-300 transition duration-100 hover:border-white hover:bg-transparent hover:text-white focus-visible:ring active:border-white active:bg-transparent md:text-base"
        >
          Contactez-nous
        </Link>
      </div>
    </>
  );
};

export default SectionTitle;
