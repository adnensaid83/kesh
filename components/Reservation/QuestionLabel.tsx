import React from "react";
import StepNumber from "./StepNumber";

const QuestionLabel = ({ label, step }: { label: any; step: number }) => {
  return (
    <div className="mb-4 flex items-center gap-4">
      <StepNumber step={step} />
      <p className="mb-0 font-medium uppercase text-primary">{label}</p>
    </div>
  );
};

export default QuestionLabel;
