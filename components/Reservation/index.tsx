import { FileField, QuestionReservationRecord } from "@/graphql/types/graphql";
import { Maybe } from "graphql/jsutils/Maybe";
import React from "react";
import { Image as DatoImage } from "react-datocms";
import QuoteRequest from "./ReservationRequest";
import { gfsDidot } from "../Common/SectionTitle";

type Props = {
  title: Maybe<string>;
  token?: string;
  idReservation?: string;
  reservationImage: Maybe<FileField> | undefined;
  questions: QuestionReservationRecord[];
};
const Reservation = ({
  title,
  token,
  idReservation,
  reservationImage,
  questions,
}: Props) => {
  return (
    <div className="relative mt-0 flex flex-col-reverse pb-0 pt-32 lg:flex-col lg:pb-0 lg:pt-0 xl:min-h-screen">
      <div className="inset-y-0 right-0 top-0 z-0 mx-auto w-full max-w-xl px-4 md:px-0 xl:absolute xl:mx-0 xl:mb-0 xl:w-7/12 xl:max-w-full xl:px-0 xl:pr-0">
        <svg
          className="absolute left-0 z-50 hidden h-full -translate-x-1/2 transform text-white xl:block"
          viewBox="0 0 100 100"
          fill="currentColor"
          preserveAspectRatio="none slice"
        >
          <path d="M50 0H100L50 100H0L50 0Z" />
        </svg>
        {reservationImage?.responsiveImage && (
          <DatoImage
            className="hidden h-56 w-full rounded object-cover shadow-lg md:h-96 xl:block xl:h-full xl:rounded-none xl:shadow-none"
            layout="fill"
            objectFit="cover"
            objectPosition="left"
            data={reservationImage?.responsiveImage}
          />
        )}
      </div>
      <div className="relative mx-auto flex w-full max-w-xl flex-col px-4 md:px-0 lg:px-8 xl:max-w-screen-xl">
        <div className="mb-16 lg:my-32 lg:max-w-lg">
          <h2
            className={`${gfsDidot.className} mb-8 text-4xl font-bold text-primary md:mb-12 md:text-5xl lg:mx-auto lg:max-w-2xl lg:uppercase `}
          >
            {title}
          </h2>
          <QuoteRequest
            service={questions}
            token={token || ""}
            idReservation={idReservation || ""}
          />
        </div>
      </div>
    </div>
  );
};

export default Reservation;
