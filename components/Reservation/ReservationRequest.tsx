"use client";

import { useState } from "react";
import ArrowLeft from "./ArrowLeft";
import ArrowRight from "./ArrowRight";
import { Formiz, FormizStep, useForm } from "@formiz/core";
import { isEmail, isNumber } from "@formiz/validations";
import { FieldRadio } from "./RadioOption";
import { FieldInput } from "./FieldInput";
import { FieldCheckbox } from "./CheckboxOption";
import Spin from "./spin";
import { FieldTextarea } from "./FieldTextarea";
import { QuestionI } from "./types";
import { LogLevel, buildClient } from "@datocms/cma-client-browser";
import { FieldPickIdenticalImages } from "./FieldPickIdenticalImages";
import { useRouter } from "next/navigation";
import X from "./X";
import QuestionLabel from "./QuestionLabel";
import { FieldDatePicker } from "./FieldDatePicker";
import { format } from "date-fns";
import { fr } from "date-fns/locale";
import emailjs from "@emailjs/browser";
const lastQuestion: QuestionI[] = [
  {
    name: "last",
    title: "Dernière étape",
    choice: {
      name: "",
    },
    option: [
      {
        label: "Nom et Prénom",
        value: "fullname",
        order: "1",
      },
      {
        label: "Adresse mail",
        value: "email",
        order: "2",
      },
      {
        label: "Numéro de téléphone",
        value: "phone",
        order: "3",
      },
      {
        label: "Date de l'événement",
        value: "date",
        order: "4",
      },
      {
        label: "Présentez brièvement votre projet",
        value: "description",
        order: "5",
      },
    ],
  },
];
const captcha: QuestionI[] = [
  {
    name: "twoPictures",
    title: "Sélectionnez deux images identiques",
    choice: {
      name: "",
    },
    option: [
      {
        label: "",
        value: "https://www.datocms-assets.com/132356/1718186878-fleur.jpg",
        order: "1",
      },
      {
        label: "",
        value: "https://www.datocms-assets.com/132356/1718186575-chien.jpg",
        order: "2",
      },
      {
        label: "",
        value: "https://www.datocms-assets.com/132356/1718186329-avion.jpg",
        order: "3",
      },
      {
        label: "",
        value: "https://www.datocms-assets.com/132356/1718186212-chat.jpg",
        order: "4",
      },
      {
        label: "",
        value:
          "https://www.datocms-assets.com/132356/1714991551-eventpro-min.webp",
        order: "5",
      },
      {
        label: "",
        value:
          "https://www.datocms-assets.com/132356/1714991158-fiancaille-min-min.webp",
        order: "6",
      },
      {
        label: "",
        value: "https://www.datocms-assets.com/132356/1718186329-avion.jpg",
        order: "7",
      },
      {
        label: "",
        value: "https://www.datocms-assets.com/132356/1718186575-chien.jpg",
        order: "8",
      },
      {
        label: "",
        value: "https://www.datocms-assets.com/132356/1718186212-chat.jpg",
        order: "9",
      },
    ],
  },
];
const fakeDelay = (delay = 500) => new Promise((r) => setTimeout(r, delay));
type OptionDevis = {
  question: string;
  response: string[] | string;
};
type FormValues = any;
export default function QuoteRequest({
  service,
  token,
  idReservation,
}: {
  service: any;
  token: string;
  idReservation: string;
}) {
  const [status, setStatus] = useState("idle");
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);
  const router = useRouter();
  // Fonction pour formater la date
  const formatDate = (date: Date | null) => {
    if (!date) return ""; // Si la date est null, retourne une chaîne vide
    return format(date, "dd/MM/yyyy", { locale: fr });
  };
  async function createDevisRecord({
    dataForm,
  }: {
    dataForm: {
      fullname: string;
      email: string;
      phone: string;
      date: string;
      description: string;
      details: string;
    };
  }) {
    const client = buildClient({
      apiToken: token,
      logLevel: LogLevel.BASIC,
    });
    try {
      await client.items.create({
        item_type: {
          type: "item_type",
          id: idReservation,
        },
        ...dataForm,
      });
      //console.log("Nouveau devis créé:", newDevis);
      setStatus("success");
      setShowSuccessAlert(true);
    } catch (error) {
      console.error('Erreur lors de la création du modèle "request" :', error);
    }
  }
  const sendEmail = (dataForm: any) => {
    const emailParams = {
      fullname: dataForm.fullname,
      email: dataForm.email,
      phone: dataForm.phone,
      date: dataForm.date,
      description: dataForm.description,
      details: dataForm.details,
    };
    emailjs
      .send(
        "service_bbldtwd",
        "template_fzdlxs8",
        emailParams,
        "H8W1eqTCAL8iCRYQ2"
      )
      .then(
        (response) => {
          console.log("SUCCESS!", response.status, response.text);
        },
        (error) => {
          console.error("FAILED...", error);
        }
      );
  };
  type AccI = {
    [key: string]: string | string[];
  };
  const getInitialValues = () => {
    const initialValues = service?.reduce((acc: AccI, question: QuestionI) => {
      if (question.choice.name === "unique") {
        acc[question.name] = "";
      } else if (question.choice.name === "multiple") {
        acc[question.name] = [];
      }
      return acc;
    }, {});
    return {
      ...initialValues,
      fullname: "",
      email: "",
      phone: "",
      date: "",
      description: "",
    };
  };

  const combinedQuestions = service?.concat([...captcha, ...lastQuestion]);
  const handleSubmitStep = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (
      !form.currentStep ||
      !form.currentStep?.isValid ||
      !form.currentStep?.name
    ) {
      form.submitStep();
      return;
    }

    setStatus("loading");
    await fakeDelay();
    setStatus("success");
    form.submitStep();
  };
  const form = useForm<FormValues>({
    ready: true,
    initialValues: getInitialValues(),
    onValidSubmit: async (values, form) => {
      let details: OptionDevis[] = [];

      const fieldNames = service?.map((i: QuestionI) => i.name);
      fieldNames?.forEach((fieldName: string) => {
        const value = values[fieldName as keyof FormValues];
        if (value) {
          const question = service?.filter((q: any) => q.name === fieldName)[0];
          if (question) {
            const title = question.title || "default";
            if (Array.isArray(value)) {
              details.push({
                question: title,
                response: [...value],
              });
            } else {
              details.push({
                question: title,
                response: value,
              });
            }
          }
        }
      });

      const dataForm = {
        fullname: values.fullname,
        email: values.email,
        phone: values.phone,
        date: formatDate(values.date),
        description: values.description,
        details: JSON.stringify(details),
      };
      setStatus("loading");
      await createDevisRecord({ dataForm });
      sendEmail(dataForm);
      form.setErrors({
        name: "You can display an error after an API call",
      });
      const stepWithError = form.getStepByFieldName("name");
      if (stepWithError) {
        form.goToStep(stepWithError.name);
      }
    },
  });
  const isLoading = status === "loading" || form.isValidating;

  return (
    <>
      <div className="relative">
        <Formiz connect={form}>
          <form noValidate onSubmit={handleSubmitStep} className="relative">
            {combinedQuestions?.map((question: any) => (
              <FormizStep
                name={question.name}
                key={question.name + question.title}
              >
                {question.name === "last" ? (
                  <>
                    <QuestionLabel
                      step={(form.currentStep?.index ?? 0) + 1 || 1}
                      label={question.title}
                    />
                    <div
                      className={`mb-2 grid grid-cols-1 gap-2 ${
                        question.option.length && question.option.length <= 2
                          ? "md:grid-cols-1"
                          : "md:grid-cols-2"
                      }`}
                    >
                      {question.option.map((o: any, i: number) =>
                        o.value === "email" ? (
                          <FieldInput
                            key={i}
                            name={o.value}
                            label={o.label}
                            required="Ce champ est obligatoire"
                            placeholder={o.label}
                            validations={[
                              {
                                handler: isEmail(),
                                message: "Email non valide",
                              },
                            ]}
                          />
                        ) : o.value === "description" ? (
                          <FieldTextarea
                            key={i}
                            name={o.value}
                            label={o.label}
                            placeholder={o.label}
                            required="Ce champ est obligatoire"
                          />
                        ) : o.value === "fullname" ? (
                          <FieldInput
                            key={i}
                            name={o.value}
                            label={o.label}
                            placeholder={o.label}
                            required="Ce champ est obligatoire"
                            formatValue={(val) => (val || "").trim()}
                          />
                        ) : o.value === "date" ? (
                          <FieldDatePicker
                            key={i}
                            name={o.value}
                            label={o.label}
                            placeholder={o.label}
                            required
                          />
                        ) : (
                          <FieldInput
                            key={i}
                            name={o.value}
                            label={o.label}
                            placeholder={o.label}
                            required="Ce champ est obligatoire"
                            formatValue={(val) => (val || "").trim()}
                            validations={[
                              {
                                handler: isNumber(),
                                message: "Ce champ est invalide",
                              },
                            ]}
                          />
                        )
                      )}
                    </div>
                  </>
                ) : question.name === "twoPictures" ? (
                  <>
                    <FieldPickIdenticalImages
                      name={question.name}
                      order={(form.currentStep?.index ?? 0) + 1}
                      label="SÉLECTIONNEZ DEUX IMAGES IDENTIQUES"
                      helper=""
                      options={question.option.map((q: any) => q.value)}
                    />
                  </>
                ) : question.choice.name === "unique" ? (
                  <FieldRadio
                    name={question.name}
                    order={(form.currentStep?.index ?? 0) + 1}
                    label={question.title}
                    options={question.option}
                    required="Ce champ est obligatoire "
                  />
                ) : (
                  <FieldCheckbox
                    name={question.name}
                    order={(form.currentStep?.index ?? 0) + 1}
                    label={question.title}
                    options={question.option}
                  />
                )}
              </FormizStep>
            ))}
            {!!form.steps?.length && (
              <div className="grid-cols2 grid items-center justify-between">
                {!form.isFirstStep && (
                  <button
                    className="col-start-1 h-[30px] w-[48px] rounded-md border-[3px] border-primary text-primary drop-shadow-md disabled:opacity-0 md:h-[35px] md:w-[50px]"
                    onClick={(e) => {
                      e.preventDefault();
                      form.goToPreviousStep();
                    }}
                  >
                    <ArrowLeft />
                  </button>
                )}
                {/*                 <div>
                  Step {(form.currentStep?.index ?? 0) + 1} /{" "}
                  {form.steps.length}
                </div> */}

                {form.isLastStep ? (
                  <>
                    <button
                      type="submit"
                      disabled={
                        (form.isLastStep ? !form.isValid : !form.isStepValid) &&
                        form.isStepSubmitted
                      }
                      className={"group col-start-2 text-base font-semibold"}
                    >
                      {isLoading ? (
                        <div className="group relative flex items-center gap-1 overflow-hidden rounded-full border-2 border-primary bg-white px-16 py-2 text-white duration-300 after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:z-0 after:bg-primary group-hover:text-primary group-hover:after:top-full after:motion-safe:duration-300">
                          <Spin />
                          <span className="relative z-[1] motion-safe:duration-100">
                            Envoyer
                          </span>
                        </div>
                      ) : (
                        <div className="group relative flex items-center gap-1 overflow-hidden rounded-full border-2 border-primary bg-white px-16 py-2 text-white duration-300 after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:z-0 after:bg-primary group-hover:text-primary group-hover:after:top-full after:motion-safe:duration-300">
                          <span className="relative z-[1] motion-safe:duration-100">
                            Envoyer
                          </span>
                        </div>
                      )}
                    </button>
                  </>
                ) : (
                  <button
                    type="submit"
                    disabled={
                      (form.isLastStep ? !form.isValid : !form.isStepValid) &&
                      form.isStepSubmitted
                    }
                    className={
                      "col-start-2 flex h-[30px] w-[48px] items-center justify-center rounded-md border-[3px] border-primary text-primary drop-shadow-md disabled:opacity-0 md:h-[35px] md:w-[50px]"
                    }
                  >
                    {!isLoading ? <ArrowRight /> : <Spin />}
                  </button>
                )}
              </div>
            )}
          </form>
        </Formiz>
      </div>
      {showSuccessAlert && (
        <div className="fixed left-0 top-0 z-40 flex h-full w-full  items-center justify-center bg-primary">
          <div
            className="relative mx-auto max-w-[400px] rounded-xl bg-[#F3ECE7] p-2 text-primary"
            role="alert"
          >
            <p className="m-0 px-12 py-12 text-lg ">
              Votre demande a bien été envoyée. Nous vous remercions de
              l'intérêt porté à nos services. Nous reviendrons vers vous dans
              les 48 heures.
            </p>
            <span
              className="absolute right-0 top-0 block w-10 cursor-pointer text-primary"
              onClick={() => {
                router.push("/", { scroll: false });
                setShowSuccessAlert(false);
              }}
            >
              <X />
            </span>
          </div>
        </div>
      )}
    </>
  );
}
