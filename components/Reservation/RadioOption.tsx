import { FormGroupProps } from "./FormGroup";
import { FieldProps, useField } from "@formiz/core";
import { ReactNode } from "react";
import { FormGroupOption } from "./FormGroupOption";

export type RadioOption = {
  label?: ReactNode;
  value: string;
};

type Value = RadioOption["value"];

export type FieldRadioProps<FormattedValue> = FieldProps<
  Value,
  FormattedValue
> &
  FormGroupProps & {
    options: RadioOption[];
  };

export const FieldRadio = <FormattedValue = Value,>(
  props: FieldRadioProps<FormattedValue>
) => {
  const {
    errorMessage,
    id,
    isRequired,
    setValue,
    value,
    shouldDisplayError,
    setIsTouched,
    otherProps: { options, ...rest },
  } = useField(props);

  const handleChange = (selectedValue: Value) => {
    setValue(selectedValue);
    setIsTouched(true);
  };

  const formGroupProps = {
    errorMessage,
    id,
    isRequired,
    length: options.length,
    showError: shouldDisplayError,
    ...rest,
  };
  return (
    <FormGroupOption {...formGroupProps}>
      {options.map((option) => (
        <div key={`${id}-${option.value}`}>
          <input
            type="radio"
            id={`${id}-${option.value}`}
            value={option.value}
            checked={value === option.value}
            onChange={() => handleChange(option.value)}
            className="sr-only"
          />
          <label
            htmlFor={`${id}-${option.value}`}
            className={`text-l block w-full cursor-pointer rounded-xl p-2 ${
              value === option.value
                ? "bg-primary text-white"
                : "bg-[#F3ECE7] text-primary"
            }`}
          >
            {option.label ?? option.value}
          </label>
        </div>
      ))}
    </FormGroupOption>
  );
};
