import { useState } from "react";
import { FieldProps, useField } from "@formiz/core";
import { FormGroup, FormGroupProps } from "./FormGroup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

type Value = Date | null;

export type FieldDatePickerProps = FieldProps<Value> & FormGroupProps;

export const FieldDatePicker = (props: FieldDatePickerProps) => {
  const {
    errorMessage,
    id,
    isRequired,
    setValue,
    value,
    setIsTouched,
    shouldDisplayError,
    ...rest
  } = useField(props);

  const formGroupProps = {
    errorMessage,
    id,
    isRequired,
    showError: shouldDisplayError,
    ...rest,
  };

  const [isDatePickerOpen, setIsDatePickerOpen] = useState(false);

  return (
    <FormGroup {...formGroupProps}>
      <div className="rounded-xl border-2 border-dark p-2">
        <DatePicker
          id={id}
          selected={value}
          onChange={(date) => setValue(date)}
          onFocus={() => setIsTouched(true)}
          onBlur={() => setIsTouched(true)}
          placeholderText="date de l'évenement"
          dateFormat="dd/MM/yyyy"
          showYearDropdown
          scrollableYearDropdown
          yearDropdownItemNumber={15}
          showMonthDropdown
          dropdownMode="select"
          className={`mb-0 w-full  text-base ${
            shouldDisplayError ? "border-accent" : ""
          }`}
          open={isDatePickerOpen}
          onClickOutside={() => setIsDatePickerOpen(false)}
          onCalendarOpen={() => setIsDatePickerOpen(true)}
        />
        <button
          className="hidden w-full rounded-xl border-2 border-dark p-2 text-left"
          onClick={() => setIsDatePickerOpen(true)}
        >
          Sélectionnez la date de l'événement
        </button>
      </div>
    </FormGroup>
  );
};
