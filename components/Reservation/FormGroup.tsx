import { ReactNode } from "react";
import ExclamationCircle from "./ExclamationCircle";

export type FormGroupProps = {
  children?: ReactNode;
  errorMessage?: ReactNode;
  helper?: ReactNode;
  id?: string;
  isRequired?: boolean;
  length?: number;
  label?: ReactNode;
  type?: string;
  size?: string;
  autoFocus?: boolean;
  placeholder?: string;
  order?: number;
  showError?: boolean;
};

export const FormGroup = ({
  children,
  errorMessage,
  helper,
  id,
  isRequired,
  label,
  showError,
}: FormGroupProps) => {
  return (
    <div className={`relative `}>
      {/* <label
        htmlFor={id}
        className="mb-4 block text-base font-medium text-dark"
      >
        {label} {isRequired && "*"}
      </label> */}
      <div className="">{children}</div>
      {!!helper && <span>{helper}</span>}
      {!!errorMessage && showError && (
        <div className="flex items-center gap-2 text-xs text-red-500">
          <div className="w-3">
            <ExclamationCircle />
          </div>
          {errorMessage}
        </div>
      )}
    </div>
  );
};
