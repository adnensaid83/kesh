import ExclamationCircle from "./ExclamationCircle";
import { FormGroupProps } from "./FormGroup";
import QuestionLabel from "./QuestionLabel";
import StepNumber from "./StepNumber";

export const FormGroupOption = ({
  children,
  errorMessage,
  helper,
  length,
  label,
  order,
  showError,
}: FormGroupProps) => {
  return (
    <div className="relative">
      <QuestionLabel step={order || 0} label={label} />
      <div
        className={`mb-4 grid grid-cols-1 gap-4 ${
          length && length <= 2 ? "md:grid-cols-1" : "md:grid-cols-2"
        }`}
      >
        {children}
      </div>
      {!!helper && <span>{helper}</span>}
      {!!errorMessage && showError && (
        <div className="absolute right-0 flex items-center gap-2 text-xs text-red-500">
          <div className="w-3">
            <ExclamationCircle />
          </div>
          {errorMessage}
        </div>
      )}
    </div>
  );
};
