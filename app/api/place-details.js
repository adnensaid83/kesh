// pages/api/place-details.js
import axios from "axios";

export default async function handler(req, res) {
  if (req.method === "GET") {
    const { place_id } = req.query;
    const GOOGLE_API_KEY = "AIzaSyB7XuIm6vzbXG3WTu1y9aAao9J50NJNppI";
    const url = `https://maps.googleapis.com/maps/api/place/details/json?place_id=${place_id}&language=fr&key=${GOOGLE_API_KEY}}`;

    try {
      const response = await axios.get(url);
      res.status(200).json(response.data);
    } catch (error) {
      console.error("Error fetching place details:", error.message);
      res.status(500).json({ error: "Failed to fetch place details" });
    }
  } else {
    res.setHeader("Allow", ["GET"]);
    res.status(405).end(`Method ${req.method} Not Allowed`);
  }
}
