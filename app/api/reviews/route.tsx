import axios from "axios";
import { NextRequest, NextResponse } from "next/server";

export async function GET(request: NextRequest) {
  const { searchParams } = new URL(request.url);
  const place_id = searchParams.get("place_id");

  if (!place_id) {
    return NextResponse.json(
      { error: "Missing 'place_id' parameter" },
      { status: 400 }
    );
  }

  const GOOGLE_API_KEY = "AIzaSyB7XuIm6vzbXG3WTu1y9aAao9J50NJNppI";
  const url = `https://maps.googleapis.com/maps/api/place/details/json?place_id=${place_id}&language=fr&key=${GOOGLE_API_KEY}`;

  try {
    const response = await axios.get(url);

    // Vérification que la réponse contient du JSON
    if (response.headers["content-type"]?.includes("application/json")) {
      return NextResponse.json(response.data, { status: 200 });
    } else {
      return NextResponse.json(
        { error: "Invalid response format from Google API" },
        { status: 500 }
      );
    }
  } catch (error: any) {
    console.error("Error fetching place details:", error.message);
    return NextResponse.json(
      { error: "Failed to fetch place details" },
      { status: 500 }
    );
  }
}
