"use client";
import { usePathname } from "next/navigation";

export default function Head() {
  const pathname = usePathname();
  const getTitle = () => {
    switch (pathname) {
      case "/fr/accueil":
        return "Accueil - Kesh Réception - Une magnifique salle de réception pour tous vos événement";
      case "/fr/qui-somme-nous":
        return "Qui somme nous - Kesh Réception";
      case "/fr/evenements-professionnels":
        return "Événements professionnels - Kesh Réception";
      case "/fr/evenements-prives":
        return "Événements prives - Kesh Réception";
      case "/fr/contact":
        return "Contact - Kesh Réception";
      default:
        return "Kesh Réception - Une magnifique salle de réception idéale pour tous vos événements";
    }
  };

  return (
    <>
      <title>{getTitle()}</title>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta
        name="description"
        content="Visit https://keshreception.com pour plus de details"
      />
      <link rel="icon" href="/images/favicon.ico" />
    </>
  );
}

/* export default function Head() {
  return (
    <>
      <title>
        Kesh réception une magnifique salle de réception idéale pour tous vos
        événements
      </title>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta
        name="description"
        content="Visit https://keshreception.com pour plus de details"
      />
      <link rel="icon" href="/images/favicon.ico" />
    </>
  );
} */
