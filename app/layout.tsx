import "node_modules/react-modal-video/css/modal-video.css";
import "@/styles/global.css";
import { SiteLocale } from "@/graphql/types/graphql";
import getAvailableLocales from "@/app/i18n/settings";
import Head from "./[lng]/Head";
import GoogleAnalytics from "@/components/GoogleAnalytics";
import CookieBanner from "@/components/CookieBanner";

type Params = {
  children: React.ReactNode;
  params: {
    lng: SiteLocale;
  };
};

export async function generateStaticParams() {
  const languages = await getAvailableLocales();
  return languages.map((language) => {
    language;
  });
}

export default async function RootLayout({
  children,
  params: { lng },
}: Params) {
  return (
    <html lang={lng}>
      <Head />
      <GoogleAnalytics GA_MEASUREMENT_ID="G-WCNVXSQ44R" />
      <body className={`tracking-tight antialiased`}>
        {children}
        <CookieBanner />
      </body>
    </html>
  );
}
